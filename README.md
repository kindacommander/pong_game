# pong game

'master' branch contains the original version of the game created using [this guide](https://book.amethyst.rs/stable/pong-tutorial.html) from the official [Amethyst.rs](https://amethyst.rs/) site.
'feature/pong_nightly' branch contains additional solutions necessary for the 'pong_notifier' project

## How to run

```bash
cargo run
```
## License

This repository contains free and open source software distributed under the terms of both the [MIT License](./licenses/LICENSE-MIT) and the [Apache License 2.0](./licenses/LICENSE-APACHE2.0).